#include <stdio.h>
#include "SDL.h"

#define GRAPHICS_X 640
#define GRAPHICS_Y 480

#define NUM_COLORS 5
unsigned char colors[] = { 0b00000000, 0b00000011, 0b00011100, 0b11100000, 0b11111111 };
#define COLOR_BLACK	0
#define COLOR_BLUE 	1
#define COLOR_GREEN	2
#define COLOR_RED 	3
#define COLOR_WHITE	4
int curr_color = COLOR_BLACK;

SDL_Surface *sdl_screen;
SDL_Event sdl_event;

void paint(int x, int y) {
	int i = (y * sdl_screen->pitch) + x;
	((unsigned char *)sdl_screen->pixels)[i] = colors[curr_color];
    SDL_Flip(sdl_screen);
}

void change_color() {
	curr_color = (curr_color + 1) % NUM_COLORS;
}

int main(char** argv, int argc) {
    int i;
    printf("Hello World\n");

    SDL_Init(SDL_INIT_VIDEO);
	sdl_screen = SDL_SetVideoMode(GRAPHICS_X, GRAPHICS_Y, 8, 0);

    for (i = 0; i < GRAPHICS_X * GRAPHICS_Y; i++)
    	((unsigned char *)sdl_screen->pixels)[i] = colors[COLOR_WHITE];
    SDL_Flip(sdl_screen);

    i = 1;
	while(i) {
		if(SDL_PollEvent(&sdl_event)) {
			switch(sdl_event.type) {
				case SDL_MOUSEMOTION: 
					printf("Mouse Motion\n");
					printf("\t(%d,%d) rel(%d,%d)\n", sdl_event.motion.x, sdl_event.motion.y, sdl_event.motion.xrel, sdl_event.motion.yrel);
					printf("\tl=%d, m=%d, r=%d\n", !!(sdl_event.motion.state & 1), !!(sdl_event.motion.state & 2), !!(sdl_event.motion.state & 4));
					if(sdl_event.motion.state & 1) { // left mouse button down
						paint(sdl_event.motion.x, sdl_event.motion.y);
					}
					break;
				case SDL_MOUSEBUTTONDOWN:
					printf("Mouse Button Down\n");
					printf("\t(%d,%d)%d\n", sdl_event.button.x, sdl_event.button.y, sdl_event.button.button);
					switch(sdl_event.button.button) {
						case SDL_BUTTON_LEFT:
							printf("\tleft\n");
							paint(sdl_event.button.x, sdl_event.button.y);
							break;
						case SDL_BUTTON_MIDDLE:
							printf("\tmiddle\n");
							break;
						case SDL_BUTTON_RIGHT:
							printf("\tright\n");
							change_color();
							break;
					}
					break;
				case SDL_MOUSEBUTTONUP: 
					printf("Mouse Button Up\n");
					printf("\t(%d,%d)\n", sdl_event.button.x, sdl_event.button.y);
					if(sdl_event.button.button == SDL_BUTTON_LEFT) { printf("\tleft\n"); }
					if(sdl_event.button.button == SDL_BUTTON_MIDDLE) { printf("\tmiddle\n"); }
					if(sdl_event.button.button == SDL_BUTTON_RIGHT) { printf("\tright\n"); }
					break;

				case SDL_QUIT:
					i = 0;
			}
		}
	}

	SDL_Quit();
    return 0;
}
