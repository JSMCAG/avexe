\documentclass[12pt,journal,compsoc]{IEEEtran}
\usepackage[utf8]{inputenc}

\usepackage{hyperref}

\begin{document}

\title{8086tiny: A tiny PC Emulator/Virtual Machine}


\author{Group 03

Filipe Apolinário, 70571, f.apolinario30@gmail.com

Jorge Goulart, 73882, jorgesimaoag@gmail.com

Pedro Silva, 73951, pedro.mgd.silva@hotmail.com}% <-this % stops a space

\IEEEtitleabstractindextext{%
\begin{abstract}
Our project is focused on the 8086tiny PC Emulator\cite{8086tiny}, and is divided in 2 phases: study analysis, and mouse integration.

In the present document we explore the first phase - study analysis. In particular, CPU and I/O emulation.

We also specify a possible strategy for the implementation of the mouse functionality.
\end{abstract}


\begin{IEEEkeywords}
8086tiny, PC emulator, Whole-System VM, x86, Study Analysis, I/O Emulation, CPU Emulation, Mouse Integration
\end{IEEEkeywords}}


% make the title area
\maketitle

\IEEEdisplaynontitleabstractindextext
\IEEEpeerreviewmaketitle
\section{Introduction}
\IEEEPARstart
8086tiny is a free, open source PC XT-compatible emulator/virtual machine written in C. It is, we believe, the smallest of its kind (the fully-commented source is around 28K). Despite its size, 8086tiny provides a highly accurate 8086 CPU emulation, together with support for PC peripherals including XT-style keyboard, floppy/hard disk, clock, timers, audio, and Hercules/CGA graphics. 8086tiny is powerful enough to run software like AutoCAD, Windows 3.0, and legacy PC games.
%THIS INTRO WAS COPY-PASTED FROM http://www.megalith.co.uk/8086tiny/

For this delivery, our main goal was to study and understand the 8086tiny Virtual Machine, focusing on how the CPU and the I/O were Emulated; and what was already implemented that could help us in implementing the mouse functionality.


\section{CPU Emulation}

\subsection{Registers and Flags}
All registers are, in fact, stored in the guest virtual memory, next to the available guest RAM (1MB).

In the Intel 8086 processor, all registers were 16-bit long. However, registers Ax, Bx, Cx and Dx could also be accessed as the double amount of 8-bit long registers (for example, Ax could be accessed as Ah and Al).

In order to emulate this feature, the 8086tiny emulator stores two pointers (regs8 and regs16) to the same address ($mem + REGS$\textunderscore$BASE$) but have different sizes.
All other registers are also included in the reg16 array, with the exception of the IP, which is stored in a global variable.

All flags are stored in the same buffer, after the registers.


\subsection{Instructions}
To simplify the process of selecting the correct routine to run for each instruction, the 8086tiny has some functions to translate the guest instructions into more generic opcodes. Each one encapsulates a set of different, yet similar, instructions of the original processor.

In order to be able to translate this instructions, the emulator has, in it's virtual BIOS, a lookup table. Each entry in this table corresponds to the translated opcode (called XLat Opcode).

This table is loaded when loading the virtual BIOS.

To differentiate between the several instructions that correspond to the same XLat Opcode, other similar techniques are used.

\subsection{Interruptions}
When an interruption is called (whether by the INT instruction, or through hardware), the 8086tiny calls a function that emulates the behavior of the original processor. This means that, whenever there is an interruption, the emulator follows the following steps:

\begin{enumerate}
\item{Stores the flags in the guest's stack}
\item{Stores the CS and IP registers in the guest's stack}
\item{Loads the pointer to the interrupt routine, using the guest's lookup table, or the virtual BIOS, in case it hasn't been redefined}
\item{Stores the pointer in the IP register}
\item{Sets the IF and TF flags to 0}
\end{enumerate}

Since the IP register has been changed to point to the beginning of the interruption routine, the next instruction to be executed will be from this routine.


\subsection{Emulation Phases}

\subsubsection{BootUp}
When first starting up the emulator, some steps have to be taken in order to correctly set the starting state of the virtual machine.

\begin{enumerate}
\item{In case the user selected to enable graphics mode, the emulator initializes the SDL Graphics library}
\item{In the runtime arguments, the user can specify the location of a floppy disk image, and hard drive image.
These two, along side with the virtual BIOS, are loaded into memory}
\item{Sets the IP and CS registers to the beginning of the BIOS}
\item{Loads the virtual BIOS translation lookup tables}
\end{enumerate}


\subsubsection{Execution Loop}
The 8086tiny, being a simple emulator, uses a Decode-and-dispatch interpreter in it's main execution loop.
While this is considered to have a slow performance, the guest specifications are much lower than today's hardware, so the emulator can still emulate the guest code without performance issues.

During the main loop, it follow these steps:
\begin{enumerate}
\item{It loads and translates the guest instruction into a XLat Opcode}
\item{Using the XLat Opcode, it selects which set of instructions are needed to be executed, and executes them}
\item{Updates the IP}
\item{Updates the flags that need to be updated}
\item{At each KEYBOARD\textunderscore TIMER\textunderscore UPDATE\textunderscore DELAY, it checks if there were any keyboard interactions, and calls INT 7 if that is the case}
\item{At each GRAPHICS\textunderscore UPDATE\textunderscore DELAY, it updates the graphical SDL screen}
\item{If TF is 1, call INT 1}
\end{enumerate}

The loop ends when $CS:IP == 00:00$


\section{I/O Emulation}

\subsection{Keyboard}

\subsubsection{Capturing events}
The emulator has two distinct states: a graphical mode and a text mode.

While in graphical mode, SDL is used to catch keyboard events. In text mode, the system call read(int fd, void *buf, size\textunderscore t count) is used instead.

The keyboard is checked every KEYBOARD\textunderscore TIMER\textunderscore UPDATE\textunderscore DELAY instructions, and, if an event is catched, the pressed key is stored in the guest's memory, and a call to the keyboard driver (INT 7) is made.

\subsubsection{Keyboard Driver}
The key presses in the host machine do not have a 1:1 correspondence to key presses in the guest machine. For example, in order to simulate a press to F3 in the guest, a user must use Crtl + F followed by pressing 3.

In order to allow this behavior, the virtual BIOS has a special keyboard driver that is called with INT 7. This driver takes the real key presses and, based on predefined rules, translates them into guest key presses.

\subsection{Screen}
When the emulator first starts up, it is in text mode. However, applications can change to graphical mode by calling INT 10h.

While in text mode, 8086tiny uses the system call write(int fd, const void *buf, size\textunderscore t nbytes) to write to stdout.

When first starting graphical mode, 8086tiny creates a new SDL window. It is then populated with info from the guest's video memory every GRAPHICS\textunderscore UPDATE\textunderscore DELAY instructions.

If the guest deactivates graphical mode, then the SDL window is destroyed.


\subsection{Audio}
The 8086tiny uses SDL to emulate the PC speaker.

SDL uses a callback system for the audio. The callback back function in the emulator takes the data from the guest's virtual audio port, and uses it to replicate the PC speaker sound.


\subsection{Disk}
To simulate the disk, the emulator uses the function call lseek(int fd, off\textunderscore t offset, int whence) to point to the correct position in the disk drive. Then, it uses read(int fd, void *buf, size\textunderscore t count) and write(int fd, const void *buf, size\textunderscore t nbytes) to read or write to the hard disk or floppy disk image.


\section{Implementation Strategies}
To implement the mouse functionality, we are going to use the SDL mouse support. This limits our implementation to the graphical mode, meaning that there will be no changes while in text mode.

To map the host's mouse input to the guest's mouse, we are going to use a similar strategy to the one used for the graphical screen.

\section{Conclusion}
In this phase we have concluded the study of the 8086tiny emulator. With this, we are able to start the implementation of the mouse functionality.
The project is, thus, on track.


\begin{thebibliography}{9}
\bibitem{8086tiny}
8086tiny Homepage
\\\url{http://www.megalith.co.uk/8086tiny/index.html}

\bibitem{download}
8086tiny Download Page
\\\url{http://www.megalith.co.uk/8086tiny/download.html}

\bibitem{repository}
8086tiny GitHub Repository
\\\url{https://github.com/adriancable/8086tiny}

\bibitem{forum}
8086tiny Forum
\\\url{http://8086tiny.freeforums.net/}

\bibitem{license}
MIT License
\\\url{http://www.megalith.co.uk/8086tiny/license.txt}
\end{thebibliography}

\end{document}